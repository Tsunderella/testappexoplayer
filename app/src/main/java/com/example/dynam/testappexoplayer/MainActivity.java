package com.example.dynam.testappexoplayer;

import android.app.Activity;
import android.graphics.SurfaceTexture;
import android.media.MediaCodec;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.dynam.testappexoplayer.R;
import com.google.android.exoplayer.DefaultLoadControl;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.LoadControl;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecSelector;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.hls.DefaultHlsTrackSelector;
import com.google.android.exoplayer.hls.HlsChunkSource;
import com.google.android.exoplayer.hls.HlsPlaylist;
import com.google.android.exoplayer.hls.HlsPlaylistParser;
import com.google.android.exoplayer.hls.HlsSampleSource;
import com.google.android.exoplayer.hls.PtsTimestampAdjusterProvider;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;
import com.google.android.exoplayer.util.ManifestFetcher;

import java.io.IOException;
import java.util.Formatter;
import java.util.Locale;



public class MainActivity extends Activity {

    private SurfaceView surfaceView;
    private TextureView surface, surface2;
    private ExoPlayer exoPlayer,exoPlayer2;
    private boolean bAutoplay=true,bAutoplay2=true;
    private boolean bIsPlaying=false,bIsPlaying2=false;
    private boolean bControlsActive=true;
    private ImageButton btnPlay;
    private ImageButton btnFwd;
    private ImageButton btnPrev;
    private ImageButton btnRew;
    private ImageButton btnNext;
    private int RENDERER_COUNT = 300000;
    private int minBufferMs =    250000;

    private final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private final int BUFFER_SEGMENT_COUNT = 256;
    private LinearLayout mediaController;
    private SeekBar seekPlayerProgress;
    private Handler handler;
    private TextView  txtCurrentTime;
    private TextView txtEndTime;
    private StringBuilder mFormatBuilder;
    private Formatter mFormatter;
    private String HLSurl = "http://walterebert.com/playground/video/hls/sintel-trailer.m3u8";
    private String mp4URL = "http://www.sample-videos.com/video/mp4/480/big_buck_bunny_480p_5mb.mp4";
    private String userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:40.0) Gecko/20100101 Firefox/40.0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player_main_layout);

        initView();



    }
    // Instanciamos y validamos que los TextureView esten iniciados
    private void initView(){
        surface = (TextureView) findViewById(R.id.surface);
        surface2 = (TextureView) findViewById(R.id.surface2);
       // surface.setSurfaceTextureListener(this);
        //surface2.setSurfaceTextureListener(this);

        //Listener para cambiar que textureview esta en frente
        surface.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                surface.bringToFront();
                return true;
            }
        });
        surface2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                surface2.bringToFront();
                return true;
            }
        });
        //Listener para verificar que el textureview esta listo para que sea usado en exoplayer
        surface.setSurfaceTextureListener(new TextureView.SurfaceTextureListener(){

            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
                //Creamos un surface para el constructor de exoplayer
                Surface surf = new Surface(surfaceTexture);
                //creamos un player y se lo pasamos al metodo que crea y lo ejecuta, mandamos el surface, el player y la url del video
                exoPlayer = ExoPlayer.Factory.newInstance(RENDERER_COUNT);
                initHLSPlayer(0, surf, exoPlayer, HLSurl);

                //play when ready
                if(bAutoplay){
                    if(exoPlayer!=null){
                        exoPlayer.setPlayWhenReady(true);
                        bIsPlaying=true;
                        //setProgress();
                    }else{
                        System.out.println("NOPE");
                    }

                }
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        });
        //listener para el segundo surface y el nuevo exoplayer
        surface2.setSurfaceTextureListener(new TextureView.SurfaceTextureListener(){

            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
                Surface surf = new Surface(surfaceTexture);
                exoPlayer2 = ExoPlayer.Factory.newInstance(RENDERER_COUNT);
                initHLSPlayer(0, surf, exoPlayer2,HLSurl);
                if(bAutoplay2){
                    if(exoPlayer2!=null){
                        exoPlayer2.setPlayWhenReady(true);
                        bIsPlaying=true;
                       // setProgress();
                    }else{
                        System.out.println("NOPE");
                    }

                }
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        });
    }
    //Inicia los mediacontrols, pero los deshabilite al poner 2 videos utilizando el mismo source de render.
    private void initMediaControls() {
        initSurfaceView();
        initPlayButton();
        initSeekBar();
        initTxtTime();
        initFwd();
        initPrev();
        initRew();
        initNext();

    }
//control deshabilitado
    private void initNext() {
        btnNext = (ImageButton) findViewById(R.id.next);
        btnNext.requestFocus();
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exoPlayer.seekTo(exoPlayer.getDuration());
                exoPlayer2.seekTo(exoPlayer2.getDuration());
            }
        });
    }
    //control deshabilitado
    private void initRew() {
        btnRew = (ImageButton) findViewById(R.id.rew);
        btnRew.requestFocus();
        btnRew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exoPlayer.seekTo(exoPlayer.getCurrentPosition()-10000);
                exoPlayer2.seekTo(exoPlayer2.getCurrentPosition()-10000);
            }
        });
    }
    //control deshabilitado
    private void initPrev() {
        btnPrev = (ImageButton) findViewById(R.id.prev);
        btnPrev.requestFocus();
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exoPlayer.seekTo(0);
                exoPlayer2.seekTo(0);
            }
        });


    }

    //control deshabilitado
    private void initFwd() {
        btnFwd = (ImageButton) findViewById(R.id.ffwd);
        btnFwd.requestFocus();
        btnFwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exoPlayer.seekTo(exoPlayer.getCurrentPosition()+10000);
                exoPlayer2.seekTo(exoPlayer2.getCurrentPosition()+10000);
            }
        });


    }
    //control deshabilitado
    private void initTxtTime() {
        txtCurrentTime = (TextView) findViewById(R.id.time_current);
        txtEndTime = (TextView) findViewById(R.id.player_end_time);
    }
    //control deshabilitado
    private void initSurfaceView() {
        surface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleMediaControls();
            }
        });
    }
    //control deshabilitado
    private String stringForTime(int timeMs) {
        mFormatBuilder = new StringBuilder();
        mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
        int totalSeconds =  timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours   = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }
    //control deshabilitado
    private void setProgress() {
        seekPlayerProgress.setProgress(0);
        seekPlayerProgress.setMax(0);
        seekPlayerProgress.setMax((int) exoPlayer.getDuration()/1000);


        handler = new Handler();
        //Make sure you update Seekbar on UI thread
        handler.post(new Runnable() {

            @Override
            public void run() {
                if (exoPlayer != null && bIsPlaying ) {
                    seekPlayerProgress.setMax(0);
                    seekPlayerProgress.setMax((int) exoPlayer.getDuration()/1000);
                    int mCurrentPosition = (int) exoPlayer.getCurrentPosition() / 1000;
                    seekPlayerProgress.setProgress(mCurrentPosition);
                    txtCurrentTime.setText(stringForTime((int)exoPlayer.getCurrentPosition()));
                    txtEndTime.setText(stringForTime((int)exoPlayer.getDuration()));

                    handler.postDelayed(this, 1000);
                }

            }
        });


    }
    //control deshabilitado
    private void initSeekBar() {
        seekPlayerProgress = (SeekBar) findViewById(R.id.mediacontroller_progress);
        seekPlayerProgress.requestFocus();

        seekPlayerProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser) {
                    // We're not interested in programmatically generated changes to
                    // the progress bar's position.
                    return;
                }

                exoPlayer.seekTo(progress*1000);
                exoPlayer2.seekTo(progress*1000);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekPlayerProgress.setMax(0);
        seekPlayerProgress.setMax((int) exoPlayer.getDuration()/1000);


    }

    //control deshabilitado
    private void toggleMediaControls() {

        if(bControlsActive){
            hideMediaController();
            bControlsActive=false;

        }else{
            showController();
            bControlsActive=true;
            setProgress();
        }
    }
    //control deshabilitado
    private void showController() {
        mediaController.setVisibility(View.VISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    //control deshabilitado
    private void hideMediaController() {
        mediaController.setVisibility(View.GONE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    //control deshabilitado
    private void initPlayButton() {
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        btnPlay.requestFocus();
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bIsPlaying){
                    exoPlayer.setPlayWhenReady(false);
                    //exoPlayer2.setPlayWhenReady(false);
                    bIsPlaying=false;
                }else{
                    exoPlayer.setPlayWhenReady(true);
                   // exoPlayer2.setPlayWhenReady(true);
                    bIsPlaying=true;
                    setProgress();
                }
            }
        });
    }


//Metodo principal, que recibe un surface, un exoplayer, la url del video. Crea el datasource, crea los render de audio y video.
    private void initHLSPlayer(int position,final Surface surf, final ExoPlayer exoplayer, final String HLSurl) {
        Handler mHandler= new Handler();

        final ManifestFetcher<HlsPlaylist> playlistFetcher;
        HlsPlaylistParser parser = new HlsPlaylistParser();
        playlistFetcher = new ManifestFetcher<>(HLSurl,
                new DefaultUriDataSource(this, userAgent), parser);


        playlistFetcher.singleLoad(mHandler.getLooper(), new ManifestFetcher.ManifestCallback<HlsPlaylist>() {


            @Override
            public void onSingleManifest(HlsPlaylist manifest) {

                LoadControl loadControl = new DefaultLoadControl(new DefaultAllocator(BUFFER_SEGMENT_SIZE));
                DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                PtsTimestampAdjusterProvider timestampAdjusterProvider = new PtsTimestampAdjusterProvider();
                DataSource dataSource = new DefaultUriDataSource(MainActivity.this, bandwidthMeter, userAgent);
                HlsChunkSource chunkSource = new HlsChunkSource(true , dataSource, HLSurl, playlistFetcher.getManifest(),
                        DefaultHlsTrackSelector.newDefaultInstance(MainActivity.this), bandwidthMeter, timestampAdjusterProvider,
                        HlsChunkSource.ADAPTIVE_MODE_SPLICE);
                HlsSampleSource sampleSource = new HlsSampleSource(chunkSource, loadControl,
                        BUFFER_SEGMENT_COUNT * BUFFER_SEGMENT_SIZE);
                MediaCodecVideoTrackRenderer videoRenderer = new MediaCodecVideoTrackRenderer(MainActivity.this, sampleSource,
                        MediaCodecSelector.DEFAULT, MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT);
                MediaCodecAudioTrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource,
                        MediaCodecSelector.DEFAULT);

//Prepara el player recibido con los render de video
                exoplayer.prepare(videoRenderer, audioRenderer);
                exoplayer.sendMessage(videoRenderer,
                        MediaCodecVideoTrackRenderer.MSG_SET_SURFACE,
                        surf);
                exoplayer.seekTo(0);
                //initMediaControls();



            }

            @Override
            public void onSingleManifestError(IOException e) {

            }
        });
    }




}
